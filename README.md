# make101 API

## Local development 
Use docker-compose, this builds a development version of the api, initializes the database in a containers and starts pgadmin in a ocntainer for a ui for the db:  

    docker-compose up 

## Deployment
Gitlab is used for deployment. On every push of the master-branch, a new version of the docker container is created, using Gitlab CI (as defined in `.gitlab-ci.yml`). This new container can be pulled by the serverwhere this container is running. 

    git push remote master 

## Server installation 
This container is running on a Digital Ocean Droplet. The `docker-compose.production.yml` file in this repository is used for production use. The `ecosystem.config.js` is used for PM2 configuration. A `.env` file is used to hold the environment variables. 

* Create a database and user with credentials 
* Create sendgrid api-key and html template 
* Use the network of the docker databse on the server `docker networks ls` as hostname
* Create Sentry.io project 
* Create auth0 project
* Install: 

      mkdir -p /var/www/make101/make101_api
      cd /var/www/make101/make101_api
      cp docker-compose.production.yml ./docker-compose.yml 
      cp ecosystem.config.js ./
      cp env.sample ./.env
      nano .env # update values 

      docker-compose pull 
      docker-compose up -d

Now the API is running. Update NGINX configuration to make the API accessible from the web.

    sudo cp .config/nginx.conf /var/www/sites-available/api.make101.urbanlink.nl.conf 
    sudo ln -s /etc/nginx/sites-available/api.make101.urbanlink.nl.conf /etc/nginx/sites-enabled/api.make101.urbanlink.nl.conf
    # make sure the domain is accessible from the web, so certbot validation resolves.
    sudo mkdir /etc/letsencrypt/live/api.make101.urbanlink.nl/
    sudo openssl dhparam -out /etc/letsencrypt/live/api.make101.urbanlink.nl/dhparam2048.pem 2048
    sudo certbot # select domain and redirect to 443
    
    sudo service nginx reload
    sudo service nginx restart 

Now the API is Live! 

--- 
db migrations: https://wanago.io/2019/01/28/typeorm-migrations-postgres/ 