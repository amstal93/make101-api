import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent
} from 'typeorm'
import logger from '../utils/logger'
import ToMake from '../entities/tomake/tomake.entity'
import { CreateItemSlug } from '../services/tomake.service'

@EventSubscriber()
export class ToMakeSubscriber implements EntitySubscriberInterface<ToMake> {
  
  listenTo() {
    return ToMake;
  }

  async afterInsert(event: InsertEvent<ToMake>) {
    let item: ToMake = event.entity;
    logger.info(`[ tomake.subscriber ] afterInsert - Create slug for ${item.id}`)
    item.slug = CreateItemSlug(item)
    logger.info(`[ tomake.subscriber ] afterInsert - Slug for ${item.id}: ${item.slug}`)
    await event.manager.getRepository(ToMake).save(item)
  }
}