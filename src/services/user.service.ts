import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import moment from 'moment';
import { getRepository } from 'typeorm';
import urlSlug from 'url-slug';
import User from '../entities/user/user.entity';
import CreateUserDto from '../entities/user/user.dto';
import logger from '../utils/logger';
import { IAuth0User } from './auth0.service';
import { 
  GenerateUniqueNickname,
  GetAuth0User, 
  UpdateAuth0User
} from '../services/auth0.service';


/**
 *
 * Find user and fetch from auth0 if not exists
 *
 **/
const GetUser = async (sub: string): Promise<User> => {
  let user: User;

  // Fetch user from database
  try {
    user = await getRepository(User).findOne({ where: { auth0: sub } });
  } catch (err) {
    logger.warn(`[user.service] GetUser - error querying user`, err);
    return Promise.reject(err);
  }
  if (user) { return user; }

  // User not found, try to fetch it from Auth0 
  logger.debug('[ user.service ] GetUser - User not found, get user from auth0');
  let auth0User = await GetAuth0User(`${sub}`);
  if (!auth0User) { return null; }

  logger.debug('[ user.service ] GetUser - User fetched from auth0, sync to local db');
  const userDto:CreateUserDto = await CastAuth0ToUser(auth0User);

  logger.debug('[ user.service ] GetUser - UserDTO created, saving user');
  try {
    user = await CreateUser(userDto);
  } catch(err) {
    logger.warn('[ user.service ] GetUser - Error saving user', err);
    return Promise.reject(err);
  }

  return user;
}

/**
 *
 * Convert Auth0 user to local db user object
 *
 **/
const CastAuth0ToUser = async (auth0user: IAuth0User): Promise<CreateUserDto> => {

  logger.debug(`[ user.service ] CastAuth0ToUser - Initiating user mapping. `);

  // Make sure there is a nickname
  if (!auth0user.nickname) {
    logger.debug(`[ user.service ] CastAuth0ToUser - Nickname is not present, creating from email address.`);
    let nick = auth0user.email.split('@')[0];
    logger.debug(`[ user.service ] CastAuth0ToUser - New nickname created: ${nick}`);

    logger.debug(`[ user.service ] CastAuth0ToUser - Generate a unique new nickname: ${nick}`);
    auth0user.nickname = await GenerateUniqueNickname(nick);
    logger.debug(`[ user.service ] CastAuth0ToUser - New unique nickname created: ${nick}`);

    await UpdateAuth0User(auth0user.user_id, { nickname: auth0user.nickname })
  }
  // initiate metadata 
  auth0user.app_metadata = auth0user.app_metadata || {};
  auth0user.user_metadata = auth0user.user_metadata || {};
  // make sure the user has a slug 
  if (!auth0user.app_metadata.slug) {
    const slug = urlSlug(auth0user.nickname);
    logger.debug(`[ user.service ] CastAuth0ToUser - Add new slug ${slug} to app_metadata`);
    auth0user.app_metadata.slug = slug; 
    await UpdateAuth0User(auth0user.user_id, { app_metadata: { slug: auth0user.app_metadata.slug } });
  }

  if (!auth0user.app_metadata.account_plan) {
    auth0user.app_metadata.account_plan = 'basic';
    await UpdateAuth0User(auth0user.user_id, { app_metadata: { account_plan: auth0user.app_metadata.account_plan } });
  }

  // Create the new user object.
  const newUser: CreateUserDto = {
    auth0           : auth0user.user_id,
    email           : auth0user.email,
    email_verified  : auth0user.email_verified, 
    nickname        : auth0user.nickname,
    slug            : auth0user.app_metadata.slug,
    firstname       : auth0user.user_metadata.firstname,
    lastname        : auth0user.user_metadata.lastname,
    bio             : auth0user.user_metadata.bio,
    picture         : auth0user.picture,
    account_plan    : auth0user.app_metadata.account_plan,
    account_plan_end: auth0user.app_metadata.account_plan_end,
    admin           : auth0user.app_metadata.admin || false,
    signed_up       : moment(auth0user.created_at).toDate()
  }

  logger.debug(`[ user.service ] CastAuth0ToUser - New user DTO created`);4  
  
  return newUser;
}

/**
 *
 * Create a new user in the local database.
 *
 **/
async function CreateUser(newUser: CreateUserDto): Promise<User> {
  
  // convert to userdto class 
  newUser = plainToClass(CreateUserDto, newUser);
  // validate incoming user data
  const errors = await validate(newUser);
  if (errors.length) {
    logger.warn('User create error', errors);
    return Promise.reject(errors);
  }
  // Start saving the new user
  let user: User;
  try {
    // Create a new db entry
    user = getRepository(User).create(newUser);
    // Save the new user
    await getRepository(User).save(newUser);
    logger.info(`saved new user ${user.nickname} (${user.email})`);
  } catch (err) {
    // Something went wrong, return error
    return Promise.reject(err);
  }
  // return the result
  return user;
}

export { 
  CreateUser, 
  GetUser,
  CastAuth0ToUser
}