import 'reflect-metadata';
import { createConnection, Connection } from 'typeorm';
import express from "express";
import * as controllers from './controllers';
import Controller from 'interfaces/controller.interface';
import {initCron} from './cron';
import { config } from './config';
import "reflect-metadata";
import * as Sentry from '@sentry/node';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';
import jwt from 'express-jwt';
import errorMiddleware from './middleware/error.middleware';
import { FixItems} from './services/tomake.service'

const bootstrap = async () => {
  try {

    const conn: Connection = await createConnection();
    // await conn.runMigrations();
    if (config.db.sync) {
      await conn.synchronize(true); 
    }

    const app = express();

    // Call midlewares
    if (config.sentry.dsn) {
      app.use(Sentry.Handlers.requestHandler());
    }

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json({ limit: '5mb' }));
    
    let SIX_MONTHS = 1000 * 60 * 60 * 24 * 31 * 6;
    app.use(helmet.frameguard());
    app.use(helmet.xssFilter());
    app.use(helmet.noSniff());
    app.use(helmet.ieNoOpen());
    app.use(helmet.hsts({ maxAge: SIX_MONTHS, includeSubDomains: true, force: true }));
    app.use(helmet.noCache());
    app.disable('x-powered-by');

    app.use(cors({ maxAge: 1728000 }));

    // Use JWT
    let jwtCheck: any = jwt(config.auth0.jwtSettings);
    app.use(jwtCheck.unless({
      path: [
        { url: '/', methods: ['GET'] },
        { url: '/admin', methods: ['GET'] },
        { url: '/admin/cron', methods: ['GET'] },
        { url: /\/item\/*/, methods: ['GET'] },
        { url: /\/list\/*/, methods: ['GET'] },
        { url: /\/widgets*/, methods: ['GET'] },
      ]
    }));

    // Helper function to remove Frameguard for widgets. 
    // Makes it possible to embed on external sites. 
    function removeFrameguard(req: express.Request, res:express.Response, next:express.NextFunction) {
      var matchUrl = '/widgets/listing/index.html';
      if (req.url.substring(0, matchUrl.length) === matchUrl) {
        res.removeHeader('X-Frame-Options');
      }
      return next(); 
    }

    // Server static files
    app.use('/', removeFrameguard, express.static('public'));

    // initiate auth service
    require('./services/auth0.service');

    // initialize controllers
    let controllersArray: Controller[] = [];
    Object.keys(controllers).forEach(key => {
      controllersArray.push(new (<any>controllers)[key]());
    });
    controllersArray.forEach((controller) => {
      app.use('/', controller.router);
    });

    // Use sentry if available
    if (config.sentry.dsn) {
      console.log('[api] - Initializing sentry Error handling');
      app.use(Sentry.Handlers.errorHandler());
    }

    app.use(errorMiddleware);

    // initialize cron
    initCron();

    FixItems();
    
    // Listen
    app.listen(process.env.NODE_PORT, () => {
      console.log(`App listening on the port ${process.env.NODE_PORT}`);
    });
  } catch (err) {
    console.warn(err);
  }
}

bootstrap();