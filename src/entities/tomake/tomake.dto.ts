import { 
  IsBoolean, 
  IsNumber, 
  IsOptional, 
  IsDate, 
  IsString, 
  Length, 
  MinLength, 
  ValidateNested 
} from 'class-validator';

class UserInToMakeDto {
  @IsNumber()
  id: number;
}

class CreateTomakeDto {

  @IsString()
  @MinLength(3)
  title: string;

  @IsOptional()
  @IsString()
  @Length(0,500)
  description?: string

  @IsOptional()
  @IsDate()
  done_date?: string

  @ValidateNested()
  user: UserInToMakeDto
}

class UpdateTomakeDto {

  @IsOptional()
  @IsString()
  @MinLength(3)
  title: string;

  @IsOptional()
  @IsString()
  @Length(0, 500)
  description: string

  @IsOptional()
  @IsBoolean()
  done: boolean
  
  @IsOptional()
  @IsDate()
  done_date: Date
}

export {
  CreateTomakeDto,
  UpdateTomakeDto
}
