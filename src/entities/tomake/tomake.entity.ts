import { 
  Entity, 
  Column,
  Index, 
  PrimaryGeneratedColumn, 
  CreateDateColumn, 
  UpdateDateColumn, 
  ManyToOne, 
  JoinColumn 
} from 'typeorm';
import { 
  IsBoolean, 
  IsDate, 
  Length 
} from "class-validator";
import User from '../user/user.entity';

@Entity()
class ToMake {

  @PrimaryGeneratedColumn()
    id: number;

  @Column({ nullable: true })
  @Index({ unique: true, where: 'slug IS NOT NULL'}) 
  @Length(3, 150)
  slug: string;

  @Column({ nullable: false })
  title: string;

  @Column({ nullable: true, type: 'text' })
  description: string;

  @Column({ default: false })
  @IsBoolean()
    done: boolean;

  @Column({ nullable: true, type: 'timestamp' })
  @IsDate()
    done_date: Date

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
    createdAt: Date

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
    updatedAt: Date

  // Associations
  @ManyToOne(_type => User, user => user.tomakes, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'user_id' })
    user: User;

}

export default ToMake;
