import * as express from 'express';
import { getRepository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import Controller from '../interfaces/controller.interface';
import ToMake from '../entities/tomake/tomake.entity';
import {
  CreateTomakeDto,
  UpdateTomakeDto
} from '../entities/tomake/tomake.dto';
import User from '../entities/user/user.entity';
import logger from '../utils/logger';
import { showToMakeItem } from '../services/tomake.service'; 
import HttpException from '../exceptions/HttpException';

class ItemController implements Controller {

  public path = '/item';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.path, this.index);
    this.router.get(`${this.path}/:slug`, this.show);
    
    this.router.post(this.path, this.create);
    
    this.router.put(`${this.path}/:id`, this.update);
    
    this.router.delete(`${this.path}/:id`, this.delete);
  }

  /**
   *
   * Create a new make item 
   * 
   **/
  private create = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    
    logger.debug(`[ item.controller ] create - new item ${req.body.title}`);
    const user = await getRepository(User).findOne({ where: { auth0: req.user.sub } });

    // Create the new entity
    let makeData:CreateTomakeDto = {
      title: req.body.title,
      description: req.body.description,
      user: user
    };
    // validate the new entity
    makeData = plainToClass(CreateTomakeDto, makeData);
    const errors = await validate(makeData); // errors is an array of validation errors
    if (errors.length > 0) {
      return next(new HttpException(400, errors.toString()));
    }

    let item: ToMake; 
    try {
      item = getRepository(ToMake).create({ ...makeData });
      await getRepository(ToMake).save(item);
    } catch (err) {
      return next(new HttpException(500, err));
    }

    res.json(item);
  }

  // /**
  //  * 
  //  * Get make-items for current logged in user. 
  //  * 
  //  **/
  // private myMakeItems = async (req: express.Request, res: express.Response, next: express.NextFunction) => {

  //   let items: ToMake[]; 
  //   console.log(req.user)
  //   if (req.headers.authorization) {
      
  //   }
  //   try {
  //     let sub: string = req.user.sub;

  //     items = await getRepository(ToMake)
  //       .createQueryBuilder('to_make')
  //       .innerJoinAndSelect('to_make.user', 'user', 'user.auth0 = :sub', { sub } )
  //       .orderBy('to_make.created_at', 'DESC')
  //       .getMany();

  //   } catch(err) { return next(new HttpException(500, err)) }
    
  //   res.json(items);
  // }

  /**
   *
   * Get public make-items 
   * 
   **/
  private index = async (req: express.Request, res: express.Response, next: express.NextFunction) => {

    let items: ToMake[];

    try {
      const q = getRepository(ToMake)
        .createQueryBuilder('to_make')
        .limit(10)
        .orderBy('to_make.created_at', 'DESC');

      if (req.query.key) {
        const sub = `auth0|${req.query.key}`;
        q.innerJoinAndSelect('to_make.user', 'user', 'user.auth0 = :sub', {sub}); 
      } else {
        q.leftJoinAndSelect('to_make.user', 'user');
      }
      
      items = await q.getMany();

    } catch(err) { return next(new HttpException(500, err)) }

    res.json(items);
  }

  /**
   *
   * Show a specific item 
   * 
   **/
  private show = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    let result: ToMake; 
    try {
      result = await showToMakeItem(req.params.slug);
    } catch (err) { return next(new HttpException(500, err)) }

    res.json(result); 
  }

  /**
   *
   * Update a Make item
   * 
   **/
  private update = async (req: express.Request, res: express.Response, next: express.NextFunction) => {

    logger.info(`[ tomake.controller ] update - item ${req.params.id}`);
    console.log(req.body)
    let item: ToMake; 

    try {
      item = await getRepository(ToMake).createQueryBuilder('to_make')
        .leftJoinAndSelect('to_make.user', 'user')
        .where('to_make.id = :mid', { mid: req.params.id })
        .andWhere('user.auth0 = :sub', { sub: req.user.sub })
        .getOne();
      if (!item) { return next(new HttpException(404, 'Item not found')); }
    } catch (err) { return new HttpException(500, err) }

    let data = req.body; 
    if ((item.done !== true ) && (req.body.done === true)) {
      data.done_date = new Date(); 
    }
    if (req.body.done === false) {
      data.done_date = null; 
    }
    let updateItem: UpdateTomakeDto = plainToClass(UpdateTomakeDto, data);
    logger.debug(`[ tomake.controller ] update - Updating with values ${JSON.stringify(data)}`)
    // validate incoming user data
    const errors = await validate(updateItem);
    if (errors.length>0) { return next(new HttpException(401, errors.toString())) }

    try{
      item = await getRepository(ToMake).save({...item, ...updateItem});
    } catch (err) { return next(new HttpException(500, err)) }
    
    logger.info(`[ tomake.controller ] update - Update done for item ${item.id}`)
    return res.json(item);
  }

  
  /**
   *
   * Delete a Make item
   * 
   **/
  private delete = async (req: express.Request, res: express.Response, next: express.NextFunction) => {

    logger.info('Delete make item ' + req.params.id);
    let result; 

    try {
      result = await getRepository(ToMake).createQueryBuilder('to_make')
        .leftJoinAndSelect(User, 'user', 'user.auth0 = :id', { id: req.user.sub } )
        .where('to_make.id = :id', { id: req.params.id })
        .delete()
        .execute();
    } catch (err) { return next(new HttpException(500, err)) }

    return res.json(result);

  }
}

export default ItemController;
