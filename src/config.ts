import jwks from 'jwks-rsa';
const version = require('./../package.json').version;

export interface IConfig {
  version: string,
	env: string,
	domain: string,
	product: {
		url: string,
		auth_url: string,
		api_url: string,
		name: string,
		author: string,
		version: string,
	},
	server: {
		url: string,
		port: number
	},
	auth0: {
		domain: string,
		clientId: string,
		clientSecret: string,
		scope: string,
		jwtSettings: {
			credentialsRequired: boolean,
			secret: any,
			audience: string,
			issuer: string,
			algorithms: [string]
		}
	},
	db: {
    sync: boolean
	},
	logger: {
		level: string,
		host: string,
		port: number
		sequelizeLevel: string,
	},
	sentry: {
		dsn: string,
	},
	email: {
		backend: 'sendgrid',
		sender: {
			default: {
				name: string,
				email: string,
			},
			support: {
				name: string,
				email: string,
			},
		},
		sendgrid: {
			secret: string,
			template: string,
		},
	},
	recaptcha: {
    key     : string,
    secret  : string,
  },
  admin: {
    name  : string,
    email : string,
  },
  mollie: {
    apikey  : string,
    webhook : string,
  }
}

const config: IConfig = {
  version: version,
	env: process.env.NODE_ENV,
	domain: process.env.DOMAIN,
	product: {
		url: process.env.PRODUCT_URL,
		auth_url: process.env.AUTH_URL,
		api_url: process.env.API_URL,
		name: process.env.PRODUCT_NAME,
		author: process.env.PRODUCT_AUTHOR,
		version: process.env.VERSION
	},
	server: {
		url: process.env.BASE_URL,
		port: Number(process.env.NODE_PORT),
	},
	auth0: {
		domain: process.env.AUTH0_DOMAIN,
		clientId: process.env.AUTH0_CLIENTID,
		clientSecret: process.env.AUTH0_CLIENTSECRET,
		scope: process.env.AUTH0_SCOPE,
		jwtSettings: {
			credentialsRequired: false,
			secret: jwks.expressJwtSecret({
				cache: true,
				rateLimit: true,
				jwksRequestsPerMinute: 5,
				jwksUri: process.env.AUTH0_JWKSURI
			}),
			audience: process.env.AUTH0_AUDIENCE,
			issuer: `https://${process.env.AUTH0_DOMAIN}/`,
			algorithms: ['RS256']
		}
	},
	db: {
	  sync: false
	},
	logger: {
		level: process.env.LOGGER_LEVEL || 'silly',
		host: process.env.LOGGER_HOST,
		port: Number(process.env.LOGGER_PORT),
		sequelizeLevel: process.env.LOGGER_SEQUELIZE_LEVEL || 'debug'
	},
	sentry: {
		dsn: process.env.SENTRY_DSN,
	},
	email: {
		backend: 'sendgrid',
		sender: {
			default: {
				name: process.env.EMAIL_SENDER_DEFAULT_NAME || 'Admin',
				email: process.env.EMAIL_SENDER_DEFAULT_EMAIL || 'info@urbanlink.nl',
			},
			support: {
				name: process.env.EMAIL_SENDER_SUPPORT_NAME || 'Admin',
				email: process.env.EMAIL_SENDER_SUPPORT_EMAIL || 'info@urbanlink.nl',
			},
		},
		sendgrid: {
			secret: process.env.EMAIL_SENDGRID_SECRET,
			template: process.env.EMAIL_SENDGRID_TEMPLATE
		},
	},
	recaptcha: {
    key     : process.env.RECAPTCHA_KEY    || '',
    secret  : process.env.RECAPTCHA_SECRET || ''
  },
  admin: {
    name  : process.env.ADMIN_NAME || 'admin',
    email : process.env.ADMIN_MAIL || 'noreply@example.com'
  },
  mollie: {
    apikey  : process.env.MOLLIE_KEY || '',
    webhook : process.env.MOLLIE_WEBHOOK || ''
  }
}

export { config }
